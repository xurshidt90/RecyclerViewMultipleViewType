package com.xurshid.example.recyclerviewmultipleviewtype.Adapter;

/**
 * Created by Xurshid on 13.10.2017.
 */
import android.content.Context;
import android.media.MediaPlayer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xurshid.example.recyclerviewmultipleviewtype.Models.Model;
import com.xurshid.example.recyclerviewmultipleviewtype.R;

import java.util.ArrayList;

public class MultiViewTypeAdapter extends RecyclerView.Adapter {

    private ArrayList dataSet;
    Context mContext;
    int total_types;
    MediaPlayer mPlayer;
    private boolean fabStateVolume = false;

    public static class TextTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtType;
        CardView cardView;

        public TextTypeViewHolder(View itemView) {
            super(itemView);

            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }

    public static class ImageTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtType;
        ImageView image;

        public ImageTypeViewHolder(View itemView) {
            super(itemView);

            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.image = (ImageView) itemView.findViewById(R.id.background);
        }
    }

    public static class AudioTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtType;
        FloatingActionButton fab;

        public AudioTypeViewHolder(View itemView) {
            super(itemView);

            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.fab = (FloatingActionButton) itemView.findViewById(R.id.fab);
        }
    }

    public static class FioTypeViewHolder extends RecyclerView.ViewHolder {

        TextView text1,text2,text3,text4,text5,text6,text7;
        ImageView img1,img2,img3,img4,img5,img6;

        public FioTypeViewHolder(View itemView) {
            super(itemView);

            this.text1 = (TextView) itemView.findViewById(R.id.text1);
            this.text2 = (TextView) itemView.findViewById(R.id.text2);
            this.text3 = (TextView) itemView.findViewById(R.id.text3);
            this.text4 = (TextView) itemView.findViewById(R.id.text4);
            this.text5 = (TextView) itemView.findViewById(R.id.text5);
            this.text6 = (TextView) itemView.findViewById(R.id.text6);
            this.text7 = (TextView) itemView.findViewById(R.id.text7);

            this.img1 = (ImageView) itemView.findViewById(R.id.img1);
            this.img2 = (ImageView) itemView.findViewById(R.id.img2);
            this.img3 = (ImageView) itemView.findViewById(R.id.img3);
            this.img4 = (ImageView) itemView.findViewById(R.id.img4);
            this.img5 = (ImageView) itemView.findViewById(R.id.img5);
            this.img6 = (ImageView) itemView.findViewById(R.id.img6);
        }
    }

    public static class IconTypeViewHolder extends RecyclerView.ViewHolder {

        TextView text1,text2,text3,text4;
        ImageView img1;

        public IconTypeViewHolder(View itemView) {
            super(itemView);

            this.text1 = (TextView) itemView.findViewById(R.id.text1);
            this.text2 = (TextView) itemView.findViewById(R.id.text2);
            this.text3 = (TextView) itemView.findViewById(R.id.text3);
            this.text4 = (TextView) itemView.findViewById(R.id.text4);

            this.img1 = (ImageView) itemView.findViewById(R.id.img1);
        }
    }
    public static class InfoTypeViewHolder extends RecyclerView.ViewHolder {

        TextView text1,text2,text3,text4,text5;

        public InfoTypeViewHolder(View itemView) {
            super(itemView);

            this.text1 = (TextView) itemView.findViewById(R.id.text1);
            this.text2 = (TextView) itemView.findViewById(R.id.text2);
            this.text3 = (TextView) itemView.findViewById(R.id.text3);
            this.text4 = (TextView) itemView.findViewById(R.id.text4);
            this.text5 = (TextView) itemView.findViewById(R.id.text5);
        }
    }

    public MultiViewTypeAdapter(ArrayList data, Context context) {
        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case Model.TEXT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.text_type, parent, false);
                return new TextTypeViewHolder(view);
            case Model.IMAGE_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_type, parent, false);
                return new ImageTypeViewHolder(view);
            case Model.AUDIO_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.audio_type, parent, false);
                return new AudioTypeViewHolder(view);
            case Model.FIO_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fio, parent, false);
                return new FioTypeViewHolder(view);
            case Model.ICON_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_icon, parent, false);
                return new IconTypeViewHolder(view);
            case Model.INFO_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_info, parent, false);
                return new InfoTypeViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        Model object = (Model) dataSet.get(position);
        switch (object.type) {
            case 0:
                return Model.TEXT_TYPE;
            case 1:
                return Model.IMAGE_TYPE;
            case 2:
                return Model.AUDIO_TYPE;
            case 3:
                return Model.FIO_TYPE;
            case 4:
                return Model.ICON_TYPE;
            case 5:
                return Model.INFO_TYPE;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        Model object = (Model) dataSet.get(listPosition);
        if (object != null) {
            switch (object.type) {
                case Model.TEXT_TYPE:
                    ((TextTypeViewHolder) holder).txtType.setText(object.text);

                    break;
                case Model.IMAGE_TYPE:
                    ((ImageTypeViewHolder) holder).txtType.setText(object.text);
                    ((ImageTypeViewHolder) holder).image.setImageResource(object.data);
                    break;
                case Model.AUDIO_TYPE:

                    ((AudioTypeViewHolder) holder).txtType.setText(object.text);

                    ((AudioTypeViewHolder) holder).fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (fabStateVolume) {
                                if (mPlayer.isPlaying()) {
                                    mPlayer.stop();

                                }
                                ((AudioTypeViewHolder) holder).fab.setImageResource(R.drawable.volume);
                                fabStateVolume = false;

                            } else {
                                mPlayer = MediaPlayer.create(mContext, R.raw.sound);
                                mPlayer.setLooping(true);
                                mPlayer.start();
                                ((AudioTypeViewHolder) holder).fab.setImageResource(R.drawable.mute);
                                fabStateVolume = true;
                            }
                        }
                    });
                    break;
                // TODO: 14.10.2017 Nimani show qilishini (set) manashu yerda yoziladi
                case Model.FIO_TYPE:
                    break;
                case Model.ICON_TYPE:
                    break;
                case Model.INFO_TYPE:
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}