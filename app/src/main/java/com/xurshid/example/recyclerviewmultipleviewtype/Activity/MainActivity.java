package com.xurshid.example.recyclerviewmultipleviewtype.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.xurshid.example.recyclerviewmultipleviewtype.Models.Model;
import com.xurshid.example.recyclerviewmultipleviewtype.Adapter.MultiViewTypeAdapter;
import com.xurshid.example.recyclerviewmultipleviewtype.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public ArrayList list;
    private RecyclerView mRecyclerView;
    private MultiViewTypeAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;

    private boolean menuType=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle(R.string.title);
        getSupportActionBar().setIcon(R.drawable.image00);

        // TODO: 13.10.2017 Manashu yerga ahamiyat berishingizni so`rayman

        getLoadData();

        adapter = new MultiViewTypeAdapter(list,this);

        linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);

        gridLayoutManager = new GridLayoutManager(this,2);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mRecyclerView.setLayoutManager(linearLayoutManager);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(adapter);
    }

    public void getLoadData() {
        list= new ArrayList();

        // TODO: 14.10.2017 Model ni optimallashtirish kerak
        
        int viewType[] = {Model.TEXT_TYPE,Model.IMAGE_TYPE,Model.AUDIO_TYPE,Model.FIO_TYPE,Model.ICON_TYPE,Model.INFO_TYPE};
        String word[] = {"Salom. Bu faqat matnli view.","Salom. Bu matnli va rasmli view.","Hey. FAB buttonni bosing va musiqa eshiting."};
        int resId[] = {0,R.drawable.custom_img,R.raw.sound};

        for (int i=0;i<100;i++)
            list.add(new Model(viewType[i%6],word[i%3],resId[i%3]));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (menuType)
        {
            item.setIcon(R.drawable.list);
            mRecyclerView.setLayoutManager(gridLayoutManager);
            menuType = false;
        }
        else {
            item.setIcon(R.drawable.grid);
            mRecyclerView.setLayoutManager(linearLayoutManager);
            menuType = true;
        }

        return super.onOptionsItemSelected(item);
    }
}